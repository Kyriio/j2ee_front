// i18n.js
import { createI18n } from 'vue-i18n';
import en_json from './locales/en.json';
import fr_json from './locales/fr.json';

const messages = {
  en: en_json,
  fr: fr_json
};

const i18n = createI18n({
  locale: 'fr',
  messages
});

export default i18n;
