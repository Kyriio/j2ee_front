import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../components/Home.vue'

import PersonnelList from '../components/PersonnelList.vue';
import PersonnelDetail from '../components/PersonnelDetail.vue';
import CreatePersonnel from '../components/CreatePersonnel.vue'; // Importez le nouveau composant
import FormationDetail from '../components/FormationDetail.vue';
import FormationList from '../components/FormationList.vue';
import CreateFormation from '../components/CreateFormation.vue';
import ExEmployeListVue from '../components/ExEmployeList.vue';
import ExEmployeDetail from '../components/ExEmployeDetail.vue';
import CreateExEmployeVue from '../components/CreateExEmploye.vue';


import absencesShow from '../components/rh_pages/Absences/ShowAbsences.vue'
import absencesCreate from '../components/rh_pages/Absences/CreateAbsences.vue'
import absencesEdite from '../components/rh_pages/Absences/UpdateAbsences.vue'

import congesCreate from '../components/rh_pages/Conges/CreateConges.vue'
import congesShow from '../components/rh_pages/Conges/ShowConges.vue'
import congesEdite from '../components/rh_pages/Conges/UpdateConges.vue'

import ListAbsences from '../components/rh_pages/Absences/ListAbsences.vue'
import ListConges from '../components/rh_pages/Conges/ListConges.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  { path: '/personnel/', component: PersonnelList },
  { path: '/personnel/:id', name: 'personnel-detail', component: PersonnelDetail, props: true },
  { path: '/create-personnel', name: 'create-personnel', component: CreatePersonnel }, // Ajoutez la nouvelle route
  { path: '/formations', component: FormationList },
  { path: '/formation/:id', name: 'formation-detail', component: FormationDetail, props: true },
  { path: '/create-formation', name: 'create-formation', component: CreateFormation },
  { path: '/exemployes', component: ExEmployeListVue},
  { path: '/exemployes/:id', name: 'exemploye-detail', component: ExEmployeDetail, props: true },
  { path: '/create-exemploye', name: 'create-exemploye', component: CreateExEmployeVue},
  /********* Absences **********/
  {
    path: '/absences/',
    name: 'ListAbsences',
    component: ListAbsences
  },{
    path: '/absences/:id/show',
    name: 'absencesShow',
    component: absencesShow,
    props: true
  },
  {
    path: '/absences/create',
    name: 'absencesCreate',
    component: absencesCreate,
    props: true
  },
  {
    path: '/absences/:id/edit',
    name: 'absencesEdite',
    component: absencesEdite,
    props: true
  },
  
  //******* Conges  ************/
  {
    path: '/conges/',
    name: 'ListConges',
    component: ListConges
  },
  {
    path: '/conges/create',
    name: 'congesCreate',
    component: congesCreate,
    props: true
  },
  {
    path: '/conges/:id/show',
    name: 'congesShow',
    component: congesShow,
    props: true
  },
  {
    path: '/conges/:id/edit',
    name: 'congesEdite',
    component: congesEdite,
    props: true
  },

];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
